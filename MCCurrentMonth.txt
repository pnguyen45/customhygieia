> db.MCCurrentMonth.find().pretty()
{
	"_id" : ObjectId("5bbcc8deaf7e821e820ba85f"),
	"_class" : "com.capitalone.dashboard.model.RallyFeature",
	"timestamp" : NumberLong("1539098820156"),
	"endDate" : "2018-10-12",
	"remainingDays" : 4,
	"startDate" : "2018-10-09",
	"lastUpdated" : NumberLong("1539098820156"),
	"collectorItemId" : ObjectId("5bb3e01067627c9bbd06db8d"),
	"plannedVelocity" : "12.0",
	"state" : "Committed",
	"taskActualTotal" : "0.0",
	"taskEstimateTotal" : "53.0",
	"taskRemainingTotal" : "40.0",
	"projectId" : "256819831864",
	"projectName" : "Sample Project",
	"planEstimate" : "12.0",
	"userListCount" : "1",
	"lastExecuted" : "2018-10-09 11",
	"options" : {
		"iterationName" : "Iteration 2 - Streamline Operations",
		"iterationId" : "256821089364",
		"instanceUrl" : "https://rally1.rallydev.com/slm/webservice/v2.0/iteration/256821089364"
	},
	"storyStages" : {
		"backlog" : "0",
		"defined" : "0",
		"inProgress" : "2",
		"completed" : "0",
		"accepted" : "0",
		"defects" : "2",
		"userStories" : {
			"storyId" : "US6",
			"storyName" : "Choose dietary plan",
			"ownerName" : "phi.p.nguyen",
			"storyUrl" : "https://rally1.rallydev.com/#/256819831864d/detail/userstory/256821089476",
			"lastUpdateDate" : NumberLong("1539112085358"),
			"state" : "In Progress",
			"storyPoints" : 5,
			"scm" : {
				"scmUrl" : "https://bitbucket.org/pnguyen45/CustomHygieia/commits/8924ece72d3f48b7b6318fde504223eed68a7ed1",
				"scmRevisionNumber" : "8924ece72d3f48b7b6318fde504223eed68a7ed1",
				"scmCommitTimestamp" : NumberLong(0),
				"numberOfChanges" : NumberLong(0)
			},
			"defects" : [
				{
					"defectId" : "DE2",
					"defectName" : "Defect found during integration tests.",
					"ownerName" : "phi.p.nguyen",
					"defectUrl" : "https://rally1.rallydev.com/slm/webservice/v2.0/defect/258883808200",
					"lastUpdateDate" : NumberLong("1539112000945"),
					"state" : "Submitted",
					"storyPoints" : 1,
					"scm" : [ ]
				},
				{
					"defectId" : "DE3",
					"defectName" : "Error case scenario fails",
					"ownerName" : "phi.p.nguyen",
					"defectUrl" : "https://rally1.rallydev.com/slm/webservice/v2.0/defect/258883823424",
					"lastUpdateDate" : NumberLong("1539112107268"),
					"state" : "Submitted",
					"storyPoints" : 1,
					"scm" : [ ]
				}
			]
		}
	},
	"storyBuildSCM" : [
		{
			"_id" : ObjectId("5bbcb078de9c1d1fb405aa9f"),
			"_class" : "com.capitalone.dashboard.model.Build",
			"collectorItemId" : ObjectId("5bb4eda267627ca47126050e"),
			"timestamp" : NumberLong("1539092600235"),
			"number" : "13",
			"buildUrl" : "http://localhost:7070/job/RunCustomeHygieia/com.capitalone.dashboard$core/13/",
			"startTime" : NumberLong("1539092591769"),
			"endTime" : NumberLong("1539092600037"),
			"duration" : NumberLong(8268),
			"buildStatus" : "Failure",
			"codeRepos" : [ ],
			"sourceChangeSet" : [
				{
					"scmRevisionNumber" : "8924ece72d3f48b7b6318fde504223eed68a7ed1",
					"scmCommitLog" : "Add collect data for defects per user story. US6",
					"scmAuthor" : "Phi Vu Nguyen",
					"scmCommitTimestamp" : NumberLong("1539092546000"),
					"numberOfChanges" : NumberLong(2)
				}
			]
		},
		{
			"_id" : ObjectId("5bbcb09ade9c1d1fb405aabb"),
			"_class" : "com.capitalone.dashboard.model.Build",
			"collectorItemId" : ObjectId("5bb4e9d867627ca471260508"),
			"timestamp" : NumberLong("1539092634482"),
			"number" : "13",
			"buildUrl" : "http://localhost:7070/job/RunCustomeHygieia/13/",
			"startTime" : NumberLong("1539092575409"),
			"endTime" : NumberLong("1539092634452"),
			"duration" : NumberLong(59043),
			"buildStatus" : "Failure",
			"codeRepos" : [
				{
					"url" : "https://bitbucket.org/pnguyen45/customhygieia",
					"branch" : "refs/remotes/origin/master",
					"type" : "GIT"
				}
			],
			"sourceChangeSet" : [
				{
					"scmRevisionNumber" : "8924ece72d3f48b7b6318fde504223eed68a7ed1",
					"scmCommitLog" : "Add collect data for defects per user story. US6",
					"scmAuthor" : "Phi Vu Nguyen",
					"scmCommitTimestamp" : NumberLong("1539092546000"),
					"numberOfChanges" : NumberLong(2)
				}
			]
		}
	]
}
{
	"_id" : ObjectId("5bbcc8deaf7e821e820ba860"),
	"_class" : "com.capitalone.dashboard.model.RallyFeature",
	"timestamp" : NumberLong("1539098820156"),
	"endDate" : "2018-10-12",
	"remainingDays" : 4,
	"startDate" : "2018-10-09",
	"lastUpdated" : NumberLong("1539098820156"),
	"collectorItemId" : ObjectId("5bb3e01067627c9bbd06db8d"),
	"plannedVelocity" : "12.0",
	"state" : "Committed",
	"taskActualTotal" : "0.0",
	"taskEstimateTotal" : "53.0",
	"taskRemainingTotal" : "40.0",
	"projectId" : "256819831864",
	"projectName" : "Sample Project",
	"planEstimate" : "12.0",
	"userListCount" : "1",
	"lastExecuted" : "2018-10-09 11",
	"options" : {
		"iterationName" : "Iteration 2 - Streamline Operations",
		"iterationId" : "256821089364",
		"instanceUrl" : "https://rally1.rallydev.com/slm/webservice/v2.0/iteration/256821089364"
	},
	"storyStages" : {
		"backlog" : "0",
		"defined" : "0",
		"inProgress" : "2",
		"completed" : "0",
		"accepted" : "0",
		"defects" : "2",
		"userStories" : {
			"storyId" : "US6",
			"storyName" : "Choose dietary plan",
			"ownerName" : "phi.p.nguyen",
			"storyUrl" : "https://rally1.rallydev.com/#/256819831864d/detail/userstory/256821089476",
			"lastUpdateDate" : NumberLong("1539112085358"),
			"state" : "In Progress",
			"storyPoints" : 5,
			"scm" : {
				"scmUrl" : "https://bitbucket.org/pnguyen45/CustomHygieia/commits/ebb26bd51bcb57e4dfdf09ad543b67a661f2e19e",
				"scmRevisionNumber" : "ebb26bd51bcb57e4dfdf09ad543b67a661f2e19e",
				"scmCommitTimestamp" : NumberLong(0),
				"numberOfChanges" : NumberLong(0)
			},
			"defects" : [
				{
					"defectId" : "DE2",
					"defectName" : "Defect found during integration tests.",
					"ownerName" : "phi.p.nguyen",
					"defectUrl" : "https://rally1.rallydev.com/slm/webservice/v2.0/defect/258883808200",
					"lastUpdateDate" : NumberLong("1539112000945"),
					"state" : "Submitted",
					"storyPoints" : 1,
					"scm" : [ ]
				},
				{
					"defectId" : "DE3",
					"defectName" : "Error case scenario fails",
					"ownerName" : "phi.p.nguyen",
					"defectUrl" : "https://rally1.rallydev.com/slm/webservice/v2.0/defect/258883823424",
					"lastUpdateDate" : NumberLong("1539112107268"),
					"state" : "Submitted",
					"storyPoints" : 1,
					"scm" : [ ]
				}
			]
		}
	},
	"storyBuildSCM" : [
		{
			"_id" : ObjectId("5bbcb17ade9c1d1fb405aabf"),
			"_class" : "com.capitalone.dashboard.model.Build",
			"collectorItemId" : ObjectId("5bb4eda267627ca47126050e"),
			"timestamp" : NumberLong("1539092858763"),
			"number" : "14",
			"buildUrl" : "http://localhost:7070/job/RunCustomeHygieia/com.capitalone.dashboard$core/14/",
			"startTime" : NumberLong("1539092828075"),
			"endTime" : NumberLong("1539092858732"),
			"duration" : NumberLong(30657),
			"buildStatus" : "Success",
			"codeRepos" : [ ],
			"sourceChangeSet" : [
				{
					"scmRevisionNumber" : "ebb26bd51bcb57e4dfdf09ad543b67a661f2e19e",
					"scmCommitLog" : "Add Defect class to correct error from earlier commit. US6",
					"scmAuthor" : "Phi Vu Nguyen",
					"scmCommitTimestamp" : NumberLong("1539092733000"),
					"numberOfChanges" : NumberLong(1)
				}
			]
		},
		{
			"_id" : ObjectId("5bbcb276de9c1d1fb405aaf5"),
			"_class" : "com.capitalone.dashboard.model.Build",
			"collectorItemId" : ObjectId("5bb4e9d867627ca471260508"),
			"timestamp" : NumberLong("1539093110523"),
			"number" : "14",
			"buildUrl" : "http://localhost:7070/job/RunCustomeHygieia/14/",
			"startTime" : NumberLong("1539092818393"),
			"endTime" : NumberLong("1539093110485"),
			"duration" : NumberLong(292092),
			"buildStatus" : "Success",
			"codeRepos" : [
				{
					"url" : "https://bitbucket.org/pnguyen45/customhygieia",
					"branch" : "refs/remotes/origin/master",
					"type" : "GIT"
				}
			],
			"sourceChangeSet" : [
				{
					"scmRevisionNumber" : "ebb26bd51bcb57e4dfdf09ad543b67a661f2e19e",
					"scmCommitLog" : "Add Defect class to correct error from earlier commit. US6",
					"scmAuthor" : "Phi Vu Nguyen",
					"scmCommitTimestamp" : NumberLong("1539092733000"),
					"numberOfChanges" : NumberLong(1)
				}
			]
		}
	]
}
{
	"_id" : ObjectId("5bbcc8deaf7e821e820ba861"),
	"_class" : "com.capitalone.dashboard.model.RallyFeature",
	"timestamp" : NumberLong("1539098820156"),
	"endDate" : "2018-10-12",
	"remainingDays" : 4,
	"startDate" : "2018-10-09",
	"lastUpdated" : NumberLong("1539098820156"),
	"collectorItemId" : ObjectId("5bb3e01067627c9bbd06db8d"),
	"plannedVelocity" : "12.0",
	"state" : "Committed",
	"taskActualTotal" : "0.0",
	"taskEstimateTotal" : "53.0",
	"taskRemainingTotal" : "40.0",
	"projectId" : "256819831864",
	"projectName" : "Sample Project",
	"planEstimate" : "12.0",
	"userListCount" : "1",
	"lastExecuted" : "2018-10-09 11",
	"options" : {
		"iterationName" : "Iteration 2 - Streamline Operations",
		"iterationId" : "256821089364",
		"instanceUrl" : "https://rally1.rallydev.com/slm/webservice/v2.0/iteration/256821089364"
	},
	"storyStages" : {
		"backlog" : "0",
		"defined" : "0",
		"inProgress" : "2",
		"completed" : "0",
		"accepted" : "0",
		"defects" : "2",
		"userStories" : {
			"storyId" : "US6",
			"storyName" : "Choose dietary plan",
			"ownerName" : "phi.p.nguyen",
			"storyUrl" : "https://rally1.rallydev.com/#/256819831864d/detail/userstory/256821089476",
			"lastUpdateDate" : NumberLong("1539112085358"),
			"state" : "In Progress",
			"storyPoints" : 5,
			"scm" : {
				"scmUrl" : "https://bitbucket.org/pnguyen45/CustomHygieia/commits/e1c0426428999b5cd9a4b706dd474bfa0607f2a2",
				"scmRevisionNumber" : "e1c0426428999b5cd9a4b706dd474bfa0607f2a2",
				"scmCommitTimestamp" : NumberLong(0),
				"numberOfChanges" : NumberLong(0)
			},
			"defects" : [
				{
					"defectId" : "DE2",
					"defectName" : "Defect found during integration tests.",
					"ownerName" : "phi.p.nguyen",
					"defectUrl" : "https://rally1.rallydev.com/slm/webservice/v2.0/defect/258883808200",
					"lastUpdateDate" : NumberLong("1539112000945"),
					"state" : "Submitted",
					"storyPoints" : 1,
					"scm" : [ ]
				},
				{
					"defectId" : "DE3",
					"defectName" : "Error case scenario fails",
					"ownerName" : "phi.p.nguyen",
					"defectUrl" : "https://rally1.rallydev.com/slm/webservice/v2.0/defect/258883823424",
					"lastUpdateDate" : NumberLong("1539112107268"),
					"state" : "Submitted",
					"storyPoints" : 1,
					"scm" : [ ]
				}
			]
		}
	},
	"storyBuildSCM" : [
		{
			"_id" : ObjectId("5bbcc381de9c1d1fb405ab39"),
			"_class" : "com.capitalone.dashboard.model.Build",
			"collectorItemId" : ObjectId("5bb4ee1e67627ca471260517"),
			"timestamp" : NumberLong("1539097473523"),
			"number" : "16",
			"buildUrl" : "http://localhost:7070/job/RunCustomeHygieia/com.capitalone.dashboard$rally-collector/16/",
			"startTime" : NumberLong("1539097468592"),
			"endTime" : NumberLong("1539097473494"),
			"duration" : NumberLong(4902),
			"buildStatus" : "Success",
			"codeRepos" : [ ],
			"sourceChangeSet" : [
				{
					"scmRevisionNumber" : "e1c0426428999b5cd9a4b706dd474bfa0607f2a2",
					"scmCommitLog" : "Correct defects counts against User Story. US6",
					"scmAuthor" : "Phi Vu Nguyen",
					"scmCommitTimestamp" : NumberLong("1539097325000"),
					"numberOfChanges" : NumberLong(1)
				}
			]
		},
		{
			"_id" : ObjectId("5bbcc475de9c1d1fb405ab69"),
			"_class" : "com.capitalone.dashboard.model.Build",
			"collectorItemId" : ObjectId("5bb4e9d867627ca471260508"),
			"timestamp" : NumberLong("1539097717657"),
			"number" : "16",
			"buildUrl" : "http://localhost:7070/job/RunCustomeHygieia/16/",
			"startTime" : NumberLong("1539097375902"),
			"endTime" : NumberLong("1539097717638"),
			"duration" : NumberLong(341736),
			"buildStatus" : "Success",
			"codeRepos" : [
				{
					"url" : "https://bitbucket.org/pnguyen45/customhygieia",
					"branch" : "refs/remotes/origin/master",
					"type" : "GIT"
				}
			],
			"sourceChangeSet" : [
				{
					"scmRevisionNumber" : "e1c0426428999b5cd9a4b706dd474bfa0607f2a2",
					"scmCommitLog" : "Correct defects counts against User Story. US6",
					"scmAuthor" : "Phi Vu Nguyen",
					"scmCommitTimestamp" : NumberLong("1539097325000"),
					"numberOfChanges" : NumberLong(1)
				}
			]
		}
	]
}
{
	"_id" : ObjectId("5bbcc8deaf7e821e820ba862"),
	"_class" : "com.capitalone.dashboard.model.RallyFeature",
	"timestamp" : NumberLong("1539098820156"),
	"endDate" : "2018-10-12",
	"remainingDays" : 4,
	"startDate" : "2018-10-09",
	"lastUpdated" : NumberLong("1539098820156"),
	"collectorItemId" : ObjectId("5bb3e01067627c9bbd06db8d"),
	"plannedVelocity" : "12.0",
	"state" : "Committed",
	"taskActualTotal" : "0.0",
	"taskEstimateTotal" : "53.0",
	"taskRemainingTotal" : "40.0",
	"projectId" : "256819831864",
	"projectName" : "Sample Project",
	"planEstimate" : "12.0",
	"userListCount" : "1",
	"lastExecuted" : "2018-10-09 11",
	"options" : {
		"iterationName" : "Iteration 2 - Streamline Operations",
		"iterationId" : "256821089364",
		"instanceUrl" : "https://rally1.rallydev.com/slm/webservice/v2.0/iteration/256821089364"
	},
	"storyStages" : {
		"backlog" : "0",
		"defined" : "0",
		"inProgress" : "2",
		"completed" : "0",
		"accepted" : "0",
		"defects" : "2",
		"userStories" : {
			"storyId" : "US8",
			"storyName" : "Order picture package",
			"ownerName" : "phi.p.nguyen",
			"storyUrl" : "https://rally1.rallydev.com/#/256819831864d/detail/userstory/256821089508",
			"lastUpdateDate" : NumberLong("1539110014548"),
			"state" : "In Progress",
			"storyPoints" : 7,
			"scm" : {
				"scmUrl" : "https://bitbucket.org/pnguyen45/CustomHygieia/commits/1770ebf8f3e1eb72911127ead62debf0384d2067",
				"scmRevisionNumber" : "1770ebf8f3e1eb72911127ead62debf0384d2067",
				"scmCommitTimestamp" : NumberLong(0),
				"numberOfChanges" : NumberLong(0)
			},
			"defects" : [ ]
		}
	},
	"storyBuildSCM" : [
		{
			"_id" : ObjectId("5bbcbc23de9c1d1fb405aaf7"),
			"_class" : "com.capitalone.dashboard.model.Build",
			"collectorItemId" : ObjectId("5bb4ecab67627ca47126050b"),
			"timestamp" : NumberLong("1539095587997"),
			"number" : "15",
			"buildUrl" : "http://localhost:7070/job/RunCustomeHygieia/com.capitalone.dashboard$Hygieia/15/",
			"startTime" : NumberLong("1539095586891"),
			"endTime" : NumberLong("1539095587965"),
			"duration" : NumberLong(1074),
			"buildStatus" : "Success",
			"codeRepos" : [ ],
			"sourceChangeSet" : [
				{
					"scmRevisionNumber" : "1770ebf8f3e1eb72911127ead62debf0384d2067",
					"scmCommitLog" : "Add sample query to reference several collections. US8",
					"scmAuthor" : "Phi Vu Nguyen",
					"scmCommitTimestamp" : NumberLong("1539095546000"),
					"numberOfChanges" : NumberLong(2)
				}
			]
		},
		{
			"_id" : ObjectId("5bbcbd51de9c1d1fb405ab2f"),
			"_class" : "com.capitalone.dashboard.model.Build",
			"collectorItemId" : ObjectId("5bb4e9d867627ca471260508"),
			"timestamp" : NumberLong("1539095889408"),
			"number" : "15",
			"buildUrl" : "http://localhost:7070/job/RunCustomeHygieia/15/",
			"startTime" : NumberLong("1539095579050"),
			"endTime" : NumberLong("1539095889384"),
			"duration" : NumberLong(310334),
			"buildStatus" : "Success",
			"codeRepos" : [
				{
					"url" : "https://bitbucket.org/pnguyen45/customhygieia",
					"branch" : "refs/remotes/origin/master",
					"type" : "GIT"
				}
			],
			"sourceChangeSet" : [
				{
					"scmRevisionNumber" : "1770ebf8f3e1eb72911127ead62debf0384d2067",
					"scmCommitLog" : "Add sample query to reference several collections. US8",
					"scmAuthor" : "Phi Vu Nguyen",
					"scmCommitTimestamp" : NumberLong("1539095546000"),
					"numberOfChanges" : NumberLong(2)
				}
			]
		}
	]
}
> 
