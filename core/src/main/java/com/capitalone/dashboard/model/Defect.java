package com.capitalone.dashboard.model;

import java.util.ArrayList;
import java.util.List;

public class Defect {
	
	private String defectId;
	private String defectName;
	private String ownerName;
	private String defectUrl;
	private long lastUpdateDate;
	private String state;
	private float storyPoints;
	
	private List<SCM> scm = new ArrayList<SCM>();

	public String getDefectId() {
		return defectId;
	}

	public void setDefectId(String defectId) {
		this.defectId = defectId;
	}

	public String getDefectName() {
		return defectName;
	}

	public void setDefectName(String defectName) {
		this.defectName = defectName;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getDefectUrl() {
		return defectUrl;
	}

	public void setDefectUrl(String defectUrl) {
		this.defectUrl = defectUrl;
	}

	public long getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(long lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public float getStoryPoints() {
		return storyPoints;
	}

	public void setStoryPoints(float storyPoints) {
		this.storyPoints = storyPoints;
	}

	public List<SCM> getScm() {
		return scm;
	}

	public void setScm(List<SCM> scm) {
		this.scm = scm;
	}
}
