package com.capitalone.dashboard.collector;


import static org.junit.Assert.assertEquals;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URI;


import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestOperations;

import static org.powermock.api.support.membermodification.MemberMatcher.method;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;

import com.capitalone.dashboard.model.RallyStoryStages;
import com.capitalone.dashboard.repository.ComponentRepository;
import com.capitalone.dashboard.repository.RallyProjectRepository;
import com.capitalone.dashboard.util.Supplier;
import com.mysema.query.annotations.Config;

@RunWith(PowerMockRunner.class)
//@RunWith(MockitoJUnitRunner.class)
@PrepareForTest(DefaultRallyClient.class)
@ContextConfiguration
public class DefaultRallyClient2Tests {


	private RallySettings rallySettings;
	private DefaultRallyClient defaultRallyClient;
	private JSONParser parser = new JSONParser();
	@Mock
	protected RallyProjectRepository rallyProjectRepository;
	@Mock
	protected Supplier<RestOperations> restOperationsSupplier;
	@Mock
	protected RestOperations rest;
	
	@Before
	public void init() {
		defaultRallyClient = PowerMockito
				.spy(new DefaultRallyClient(restOperationsSupplier, rallyProjectRepository, rallySettings));
	}

	@Test
	public void getTestStoryStages() throws IOException, ParseException, Exception {
		String changeSetsStr = getJson("rally_changesets.json");
		String defectsStr = getJson("rally_defects.json");
		String rallyStoryStages = IOUtils
				.toString(DefaultRallyClient2Tests.class.getResourceAsStream("/rally_storyStages.json"));
		
		String projName = "Sample Project";
		
		JSONObject object = (JSONObject) parser.parse(rallyStoryStages);
		JSONObject changeSetObj = (JSONObject) parser.parse(changeSetsStr);
		JSONObject defectsObj = (JSONObject) parser.parse(defectsStr);

		when(rest.exchange(Matchers.any(URI.class), eq(HttpMethod.GET), Matchers.any(HttpEntity.class),
				eq(String.class))).thenReturn(new ResponseEntity<>(getJson("rally_storyStages.json"), HttpStatus.OK));
		
		PowerMockito.doReturn(changeSetObj).when(defaultRallyClient, 
	               method(DefaultRallyClient.class, "parseAsObject", String.class, String.class))
	                .withArguments(anyString(), eq("changesets"));
		PowerMockito.doReturn(defectsObj).when(defaultRallyClient, 
	               method(DefaultRallyClient.class, "parseAsObject", String.class, String.class))
	                .withArguments(anyString(), eq("defects"));
		JSONObject storyStagesObject = (JSONObject) object.get("QueryResult");

		RallyStoryStages rallyStages = defaultRallyClient.getStoryStages(projName,
				(JSONArray) storyStagesObject.get("Results"));

		assertEquals("3", rallyStages.getAccepted());
		assertEquals(3, rallyStages.getUserStories().size());
		assertEquals("1", rallyStages.getDefects());
		assertEquals("c5e9e6a7f6b7df8504212e5aa491a232f73f162c", rallyStages.getUserStories().get(0).getScm().get(0).getScmRevisionNumber());
	}

	private String getJson(String fileName) throws IOException {
		BufferedInputStream result = (BufferedInputStream) Config.class.getClassLoader().getResourceAsStream(fileName);
		return IOUtils.toString(result);
	}

	private String str(JSONObject json, String key) {
		Object obj = json.get(key);
		return obj == null ? null : obj.toString();
	}

	private Long lng(JSONObject json, String key) {
		Object obj = json.get(key);
		return obj == null ? null : Long.valueOf(obj.toString());
	}

}
